## This repo is under development

# @universis/common
[Universis](https://universis.gr) is a coordinated effort by Greek academic institutions to build a Student Information System as an open source platform. The target is to serve our common needs to support academic and administrative processes.

**@universis/shared** package contains common components and services for building client applications for Universis project.


### Testing

#### Intro
In order to test **@universis/common** package, we use Karma as our test-runner and Jasmine as our 
testing framework. We also use **Puppeteer**, a high-level API to manipulate the dev-tools of any 
chromium based browser. You should keep in mind that puppeteer is a peer dependency of the whole project
and should be placed in the parent directory of the repository `universis`. 

#### Example

To run all of our tests simply run

```
ng test common
```

This will compile the common package and run our test suite in a `ChromeHeadless` browser with no sandbox.

#### Extra

You can also have a visual representation of the tests running by adding
```
      ChromeNoSandbox: {
        base: 'Chrome',
        flags: [
          '--no-sandbox',
          '--enable-logging=stderr',
          '--disable-web-security',
          '--disable-gpu',
          '--no-proxy-server'
        ]
      },

```
under the `customLaunchers` in `packages/common/karma.conf.js` 
