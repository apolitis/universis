import {Directive, Input} from '@angular/core';

// tslint:disable directive-selector
@Directive({
    selector: '[ngVar]',
    exportAs: 'ngVar'
})
export class NgVarDirective {

    constructor() {
    }

    [key: string]: any;

    @Input('ngVar') set assign(value: any) {
        if (value) {
            Object.assign(this, {
                $implicit: value
            });
        }
    }

    public get value() {
        return this.$implicit;
    }

    public set value(value: any) {
        this.$implicit = value;
    }

}
// tslint:enable directive-selector
