import {Pipe, PipeTransform} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {DatePipe} from '@angular/common';

/**
 *
 * Custom DatePipe representation
 * @export
 * @class LocalizedDatePipe
 * @implements {PipeTransform}
 */
@Pipe({
    name: 'localizedDate',
    pure: false
})
export class LocalizedDatePipe implements PipeTransform {

    constructor(private translateService: TranslateService) {
    }

    /**
     *
     * Converts Date value that been passed
     * @param {*} value The Date that needs to be converted
     * @param {string} [pattern='mediumDate'] Pattern of Date
     * @returns {*} Converted Date
     * @memberof LocalizedDatePipe
     */
    transform(value: any, pattern: string = 'mediumDate'): any {
        const datePipe: DatePipe = new DatePipe(this.translateService.currentLang);
        if (typeof value === 'string' && /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\+\d{2}$/.test(value)) {
            return datePipe.transform(new Date(value + ':00'), pattern);
        }
        return datePipe.transform(value, pattern);
    }
}
