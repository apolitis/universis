import { Injectable } from '@angular/core';
import {ConfigurationService} from './configuration.service';
import {AngularDataContext} from '@themost/angular';

/**
 *
 * This Service is used to get or set user storage items
 * @export
 * @class ConfigurationService
 */
@Injectable()
export class UserStorageService {

    private _hasUserStorage;

    constructor(private context: AngularDataContext) {
        //
    }

    /**
     * Validates if server supports user storage
     */
    async hasUserStorage() {
        // if flag has not been set yet
        if (this._hasUserStorage == null) {
            // ping server for user storage
            const services = await this.context.getService().execute({
                method: 'GET',
                url: 'diagnostics/services',
                headers: { },
                data: null
            });
            // if return value is an array of services
            if (Array.isArray(services)) {
                // search for user storage service
                this._hasUserStorage = services.find( service => {
                    return service.serviceType === 'UserStorageService';
                }) != null;
                return this._hasUserStorage;
            }
            // otherwise set flag to false
            this._hasUserStorage = false;
        }
        // and finally return flag
        return this._hasUserStorage;
    }

    /**
     * Gets a user storage item based on the given key
     * @param key
     */
    async getItem(key: string): Promise<any> {
        const hasUserStorage = await this.hasUserStorage();
        if (!hasUserStorage) {
            throw new Error('API server configuration does not support user storage service.');
        }
       return await this.context.getService().execute({
           method: 'POST',
           url: 'users/me/storage/get',
           headers: { },
           data: {
               key: key
           }
       });
    }

    // noinspection JSUnusedGlobalSymbols
    /**
     * Sets a user storage item
     * @param key
     * @param value
     */
    async setItem(key: string, value: any): Promise<any> {

        const hasUserStorage = await this.hasUserStorage();
        if (!hasUserStorage) {
            throw new Error('API server configuration does not support user storage service.');
        }
        return await this.context.getService().execute({
            method: 'POST',
            url: 'users/me/storage/set',
            headers: { },
            data: {
                key: key,
                value: value
            }
        });
    }

    /**
     * Removes a user storage item
     * @param key
     */
    async removeItem(key: string): Promise<any> {
        const hasUserStorage = await this.hasUserStorage();
        if (!hasUserStorage) {
            throw new Error('API server configuration does not support user storage service.');
        }
        return await this.context.getService().execute({
            method: 'POST',
            url: 'users/me/storage/set',
            headers: { },
            data: {
                key: key,
                value: null
            }
        });
    }

}
