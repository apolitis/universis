import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {AppSidebarService} from '../app-sidebar-service/app.sidebar.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styles: [`
    button.dropdown-item {
      cursor: pointer;
    }
    .sidebar .nav-dropdown-items {
      padding-left: 1rem;
    }
  `],
  encapsulation: ViewEncapsulation.None
})
export class FullLayoutComponent implements OnInit {

  public navItems = [];

  constructor(private _router: Router, private _appSidebarService: AppSidebarService) {
    //
  }

  ngOnInit(): void {
    // get sidebar navigation items
    this.navItems = this._appSidebarService.navigationItems;
  }
}
