import {Injectable} from '@angular/core';
import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class RegistrationTableConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./registrations-table.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./registrations-table.config.list.json`);
        });
    }
}

export class RegistrationTableSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./registrations-table.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./registrations-table.search.list.json`);
            });
    }
}

export class RegistrationDefaultTableConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./registrations-table.config.list.json`);
    }
}
