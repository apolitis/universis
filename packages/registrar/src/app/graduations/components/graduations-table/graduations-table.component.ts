import {Component, OnInit, ViewChild, Input, OnDestroy} from '@angular/core';
import * as GRADUATIONS_LIST_CONFIG from './graduations-table.config.list.json';
import {AdvancedTableComponent, AdvancedTableDataResult} from '../../../tables/components/advanced-table/advanced-table.component';
import { Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component.js';
import { ActivatedRoute } from '@angular/router';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-graduations-table',
  templateUrl: './graduations-table.component.html'
})
export class GraduationsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;

  public recordsTotal: any;

  public readonly config = GRADUATIONS_LIST_CONFIG;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService) { }

  ngOnInit() {
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.table.fetch(true);
      }
    });
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.table.query = null;
        this.table.ngOnInit();
      }
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

}
