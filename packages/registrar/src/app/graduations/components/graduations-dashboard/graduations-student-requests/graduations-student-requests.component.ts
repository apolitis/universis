import {Component, OnInit, ViewChild} from '@angular/core';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '../../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import * as GRADUATIONS_REQUEST_LIST_CONFIG from './graduations-requests.config.json';
import * as GRADUATIONS_REQUEST_LIST_SEARCH from './graduations-requests.search.json';
import {AdvancedSearchFormComponent} from '../../../../tables/components/advanced-search-form/advanced-search-form.component';

@Component({
  selector: 'app-graduations-student-requests',
  templateUrl: './graduations-student-requests.component.html'
})
export class GraduationsStudentRequestsComponent implements OnInit {

  public recordsTotal: any;
  @ViewChild('requests') requests: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  graduationEventId: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext

  ) { }

  async ngOnInit() {
    this.requests.query = this._context.model('GraduationRequestActions')
      .where('graduationEvent').equal(this.graduationEventId)
      .expand('student($expand=studyProgram,department,person,semester,requeststatus,inscriptionYear,inscriptionPeriod,studentStatus)')
      .prepare();

    this.requests.config = AdvancedTableConfiguration.cast(GRADUATIONS_REQUEST_LIST_CONFIG);
    this.search.form = AdvancedTableConfiguration.cast(GRADUATIONS_REQUEST_LIST_SEARCH);
    this.requests.ngOnInit();
    this.search.ngOnInit();

  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }


}
