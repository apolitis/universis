import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import * as CLASSES_LIST_CONFIG from '../classes-table/classes-table.config.list.json';
import {cloneDeep} from 'lodash';
import {TemplatePipe} from '@universis/common';

@Component({
  selector: 'app-classes-root',
  templateUrl: './classes-root.component.html',
})
export class ClassesRootComponent implements OnInit {
  public model: any;
  public classId: any;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe) { }

  async ngOnInit() {
    this.classId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('course,status,period')
      .getItem();

    // @ts-ignore
    this.config = cloneDeep(CLASSES_LIST_CONFIG as TableConfiguration);

    if (this.config.columns && this.model) {
      // get actions from config file
      this.actions = this.config.columns.filter(x => {
        return x.actions;
      })
        // map actions
        .map(x => x.actions)
        // get list items
        .reduce((a, b) => b, 0);

      // filter actions with student permissions
      this.allowedActions = this.actions.filter(x => {
        if (x.role) {
          if (x.role === 'action') {
            return x;
          }
        }
      });

      this.edit = this.actions.find(x => {
        if (x.role === 'edit') {
          x.href = this._template.transform(x.href, this.model);
          return x;
        }
      });

      this.actions = this.allowedActions;
      this.actions.forEach(action => {
        action.href = this._template.transform(action.href, this.model);
      });

    }
  }

}
