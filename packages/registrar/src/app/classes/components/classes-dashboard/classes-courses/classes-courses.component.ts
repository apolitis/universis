import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-courses',
  templateUrl: './classes-courses.component.html',
  styleUrls: ['./classes-courses.component.scss']
})
export class ClassesCoursesComponent implements OnInit {

  @Input() model: any;
  public classCourse: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {
    this.classCourse = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('course')
      .getItem();

      this.model = await this._context.model('Courses')
      .where('id').equal(this.classCourse.course.id)
      .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory')
      .getItem();
  }

}
