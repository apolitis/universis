import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-overview-general',
  templateUrl: './classes-overview-general.component.html',
  styleUrls: ['./classes-overview-general.component.scss']
})
export class ClassesOverviewGeneralComponent implements OnInit {

  public class: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.class = await this._context.model('CourseClasses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('period, status, course($expand=department)')
      .getItem();
  }

}
