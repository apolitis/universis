import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-classes-overview-exams',
  templateUrl: './classes-overview-exams.component.html',
  styleUrls: ['./classes-overview-exams.component.scss']
})
export class ClassesOverviewExamsComponent implements OnInit {

  public classExams: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.classExams = await this._context.model('CourseExamClasses')
      .where('courseClass').equal(this._activatedRoute.snapshot.params.id)
      .expand('courseExam($expand=examPeriod,status,completedByUser,year,course($expand=department))')
      .orderByDescending('courseExam/year')
      .thenByDescending('courseExam/examPeriod')
      .getItems();
  }

}
