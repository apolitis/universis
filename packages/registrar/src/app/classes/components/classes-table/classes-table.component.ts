import {Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter} from '@angular/core';
import * as CLASSES_LIST_CONFIG from './classes-table.config.list.json';
import {AdvancedTableComponent, AdvancedTableDataResult} from '../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';
import {Observable, Subscription} from 'rxjs';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component.js';
import { ActivatedRoute } from '@angular/router';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import {RequestActionComponent} from '../../../requests/components/request-action/request-action.component';
import {AdvancedRowActionComponent} from '../../../tables/components/advanced-row-action/advanced-row-action.component';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {ClientDataQueryable} from '@themost/client';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-classes-table',
  templateUrl: './classes-table.component.html',
  styles: []
})
export class ClassesTableComponent implements OnInit, OnDestroy  {

  public recordsTotal: any;
  private dataSubscription: Subscription;
  private paramSubscription: Subscription;
  private selectedItems = [];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public readonly config = CLASSES_LIST_CONFIG;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _context: AngularDataContext) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe(data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }
  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'status/alternateName as status'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(-1)
              .skip(0)
              .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.status
            };
          });
        }
      }
    }
    return items;
  }

  /**
   * Opens selected course classes
   */
  async openAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.status !== 'open';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.OpenAction.Title',
          description: 'Classes.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('open')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  /**
   * Executes open action for course classes
   */
  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      const datePublished = new Date();
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id,
          status: {
            alternateName: status
          }
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('CourseClasses').save(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  /**
   * Closes selected course classes
   */
  async closeAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items that may be opened
      this.selectedItems = items.filter( (item) => {
        return item.status !== 'closed';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Classes.CloseAction.Title',
          description: 'Classes.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
}
