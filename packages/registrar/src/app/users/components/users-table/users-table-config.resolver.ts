import {TableConfiguration} from '../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
export class UserTableConfigurationResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./users-table.config.${route.params.list}.json`)
      .catch( err => {
        return  import(`./users-table.config.list.json`);
      });
  }
}

export class UserTableSearchResolver implements Resolve<TableConfiguration> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
    return import(`./users-table.search.${route.params.list}.json`)
      .catch( err => {
        return  import(`./users-table.search.list.json`);
      });
  }
}

export class UserDefaultTableConfigurationResolver implements Resolve<any> {
  resolve(route: ActivatedRouteSnapshot,
          state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
    return import(`./users-table.config.list.json`);
  }
}
