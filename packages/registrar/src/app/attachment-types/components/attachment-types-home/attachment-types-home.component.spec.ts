import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AttachmentTypesHomeComponent } from './attachment-types-home.component';

describe('AttachmentTypesHomeComponent', () => {
  let component: AttachmentTypesHomeComponent;
  let fixture: ComponentFixture<AttachmentTypesHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AttachmentTypesHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AttachmentTypesHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
