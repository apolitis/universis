import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-students-overview-graduation',
  templateUrl: './students-overview-graduation.component.html'
})
export class StudentsOverviewGraduationComponent implements OnInit {

  public student;
  @Input() studentId: number;

  constructor(private _activatedRoute: ActivatedRoute, private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.student = await this._context.model('Students')
      .where('id').equal(this.studentId)
      .expand('person($expand=gender), department, studyProgram')
      .getItem();
    // check if student is declared and get info from studentDeclaration model
    if (this.student && this.student.studentStatus && this.student.studentStatus.alternateName === 'declared') {
      const declaredInfo = await this._context.model('StudentDeclarations')
        .where('student').equal(this.studentId)
        .getItem();
      if (declaredInfo) {
        this.student.declaredInfo = declaredInfo;
      }
    }
  }
}
