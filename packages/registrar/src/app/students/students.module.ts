import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import {StudentsRoutingModule} from './students.routing';
import {StudentsSharedModule} from './students.shared';
import { StudentsTableComponent } from './components/students-table/students-table.component';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule} from '@ngx-translate/core';
import { StudentsRootComponent } from './components/students-root/students-root.component';
import { StudentsGeneralComponent} from './components/students-dashboard/students-general/students-general.component';
import { StudentsGradesComponent} from './components/students-dashboard/students-grades/students-grades.component';
import { StudentsThesesComponent} from './components/students-dashboard/students-theses/students-theses.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import { StudentsOverviewComponent } from './components/students-dashboard/students-overview/students-overview.component';
import { StudentsCoursesComponent } from './components/students-dashboard/students-courses/students-courses.component';
import {TooltipModule} from 'ngx-bootstrap';
import { StudentsRegistrationsComponent } from './components/students-dashboard/students-registrations/students-registrations.component';
import { StudentsRequestsComponent } from './components/students-dashboard/students-requests/students-requests.component';
import { StudentsInternshipsComponent } from './components/students-dashboard/students-internships/students-internships.component';
import { StudentsScholarshipsComponent } from './components/students-dashboard/students-scholarships/students-scholarships.component';
import {ElementsModule} from '../elements/elements.module';
import {InternshipsSharedModule} from '../internships/internships.shared';
import {RegistrationPreviewFormComponent} from './components/students-dashboard/students-registrations/registration-preview-form.component';
import {RegistrationClassesFormComponent} from './components/students-dashboard/students-registrations/registration-classes-form.component';
import {RequestsSharedModule} from '../requests/requests.shared';
import {ScholarshipsSharedModule} from '../scholarships/scholarships.shared';
import { ThesesSharedModule } from './../theses/theses.shared';
import { RegistrationsSharedModule } from '../registrations/registrations.shared';
import { StudentsGraduatedComponent } from './components/students-dashboard/students-graduated/students-graduated.component';
import { StudentsMessagesComponent } from './components/students-dashboard/students-messages/students-messages.component';
import {NgPipesModule} from 'ngx-pipes';
import { StudentsCoursesByGroupComponent, GradeAveragePipe } from './components/students-dashboard/students-courses/students-courses-by-group/students-courses-by-group.component';
import {StudentsAdvancedTableSearchComponent} from './components/students-table/students-advanced-table-search.component';
import { MostModule } from '@themost/angular';
import { StudentsGraduatedAdvancedTableSearchComponent } from './components/students-dashboard/students-graduated/students-graduated-advanced-table-search.component';
import { AdvancedFormsModule } from '@universis/forms/lib/advanced-forms.module';
import {RegistrarSharedModule} from '../registrar-shared/registrar-shared.module';
import { StudentsDashboardComponent } from './components/students-dashboard/students-dashboard.component';
import {RouterModalModule} from '@universis/common/routing';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import { CoursesSharedModule } from '../courses/courses.shared';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    StudentsSharedModule,
    TablesModule,
    StudentsRoutingModule,
    ElementsModule,
    SharedModule,
    FormsModule,
    TooltipModule.forRoot(),
    InternshipsSharedModule,
    RequestsSharedModule,
    ScholarshipsSharedModule,
    ThesesSharedModule,
    NgPipesModule,
    MostModule,
    RegistrationsSharedModule,
    MostModule,
    AdvancedFormsModule,
    RegistrarSharedModule,
    RouterModalModule,
    ReportsSharedModule,
    CoursesSharedModule
  ],
  declarations: [
    StudentsHomeComponent,
    StudentsTableComponent,
    StudentsGeneralComponent,
    StudentsGradesComponent,
    StudentsThesesComponent,
    StudentsRootComponent,
    StudentsOverviewComponent,
    StudentsCoursesComponent,
    StudentsRegistrationsComponent,
    StudentsRequestsComponent,
    StudentsInternshipsComponent,
    StudentsScholarshipsComponent,
    RegistrationPreviewFormComponent,
    RegistrationClassesFormComponent,
    StudentsGraduatedComponent,
    StudentsMessagesComponent,
    StudentsCoursesByGroupComponent,
    StudentsAdvancedTableSearchComponent,
    StudentsGraduatedAdvancedTableSearchComponent,
    StudentsDashboardComponent,
    GradeAveragePipe
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudentsModule { }
