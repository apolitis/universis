import {Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter} from '@angular/core';
import * as EXAMS_LIST_CONFIG from './exams-table.config.json';
import { AdvancedTableComponent, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component.js';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import { AdvancedSearchFormComponent } from '../../../tables/components/advanced-search-form/advanced-search-form.component.js';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';
import {TranslateService} from '@ngx-translate/core';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {AdvancedRowActionComponent} from '../../../tables/components/advanced-row-action/advanced-row-action.component';
import {ClientDataQueryable} from '@themost/client';

@Component({
  selector: 'app-exams-table',
  templateUrl: './exams-table.component.html',
})
export class ExamsTableComponent implements OnInit, OnDestroy {

  public readonly config = EXAMS_LIST_CONFIG;
  private dataSubscription: Subscription;
  public recordsTotal: any;
  private selectedItems: any;
  private takeSize = 100;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _activatedTable: ActivatedTableService) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  onSelectBoxChange(event: any) {
    // get value
    const value = parseInt((<HTMLSelectElement>event.target).value, 10);
    if (isNaN(value)) {
      this._router.navigate(
        [],
        {
          relativeTo: this._activatedRoute,
          queryParams: { $filter: '' },
          queryParamsHandling: 'merge'
        }).then(() => {
          this.table.fetch();
        });
      return;
    }
    this._router.navigate(
      [],
      {
        relativeTo: this._activatedRoute,
        queryParams: { $filter: `status eq ${value}` },
        queryParamsHandling: 'merge'
      }).then(() => {
        this.table.fetch();
      });
  }

  private async takeSelectedItems(take?: number, skip?: number) {
    let items = {
      total: 0,
      skip: 0,
      value: []
    };
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          let smartTake = -1;
          let smartSkip = 0;
          if (typeof take === 'number') {
            smartTake = take;
          }
          if (typeof skip === 'number') {
            smartSkip = skip;
          }
          // get items
          const selectArguments = ['id',
            'status/alternateName as status',
            'year/id as year',
            'examPeriod/id as examPeriod'
          ];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
              .take(smartTake)
              .skip(smartSkip)
              .getList();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = {
              total: queryItems.total - this.table.unselected.length,
              skip: smartSkip,
              value: queryItems.value.filter( item => {
                return this.table.unselected.findIndex( (x) => {
                  return x.id === item.id;
                }) < 0;
              })
            };
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = {
            total: this.table.selected.length,
            skip: 0,
            value: this.table.selected.map( (item) => {
              return {
                id: item.id,
                status: item.status,
                year: item.year,
                examPeriod: item.examPeriod
              };
            })
          };
        }
      }
    }
    return items;
  }

  /**
   * Executes open action for course classes
   */
  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.total,
        success: 0,
        errors: 0
      };
      // execute promises in series within an async method
      (async () => {
        const size = this.takeSize;
        // force get selected items again
        // due to errors may be occurred while executing process again and again
        this.selectedItems = await this.takeSelectedItems(size);
        // get total records
        let total = this.selectedItems.total;
        // hold grand total (for progress bar)
        const grandTotal = total;
        let length = this.selectedItems.value.length;
        let skip = this.selectedItems.skip;
        // hold grand skip (for progress bar)
        let grandSkip = 0;
        let updated = [];
        while ((total > 0) && (skip + length <= total)) {
          // get items to update
          if (this.table.unselected && this.table.unselected.length) {
            // map all items
            updated = this.selectedItems.value.map((item) => {
              return {
                id: item.id,
                status: {
                  alternateName: status
                },
                year: item.year,
                examPeriod: item.examPeriod
              };
            });
          } else {
            // exclude unselected items
            updated = this.selectedItems.value.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            }).map((item) => {
              return {
                id: item.id,
                status: {
                  alternateName: status
                },
                year: item.year,
                examPeriod: item.examPeriod
              };
            });
          }
          try {
            // update items
            await this._context.model('CourseExams').save(updated);
            // update result success (update set have been completed)
            result.success += updated.length;
          } catch (err) {
            // update result errors (update set have been failed)
            result.errors += updated.length;
          }
          // update progress
          this.refreshAction.emit({
            progress: Math.floor(((grandSkip + size) / grandTotal) * 100)
          });
          if (this.table.smartSelect === false) {
            // exit operation because selection mode is manual
            break;
          }
          // get next items
          this.selectedItems = await this.takeSelectedItems(size);
          grandSkip += size;
          // reset variables
          total = this.selectedItems.total;
          length = this.selectedItems.value.length;
          skip = this.selectedItems.skip;
        }
      })().then(() => {
        // reload table
        this.table.fetch(true);
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async openAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        const filterStatus  = this.search.filter && this.search.filter.status;
        if (filterStatus == null || filterStatus === 'open') {
          const error = this._translateService.instant('Exams.OpenActionFilter');
          return this._modalService.showDialog(error.title, error.message);
        }
      }
      this._loadingService.showLoading();
      this.selectedItems = await this.takeSelectedItems(this.takeSize);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: new Array(this.selectedItems.total),
          modalTitle: 'Exams.OpenAction.Title',
          description: 'Exams.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('open')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async closeAction() {
    try {
      // validate search filter (on smart selection)
      if (this.table.smartSelect) {
        const filterStatus  = this.search.filter && this.search.filter.status;
        if (filterStatus == null || filterStatus === 'closed') {
          const error = this._translateService.instant('Exams.CloseActionFilter');
          return this._modalService.showDialog(error.title, error.message);
        }
      }
      this._loadingService.showLoading();
      this.selectedItems = await this.takeSelectedItems(this.takeSize);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: new Array(this.selectedItems.total),
          modalTitle: 'Exams.CloseAction.Title',
          description: 'Exams.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
}
