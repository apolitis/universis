import { Component, EventEmitter, Input, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '../../../tables/components/advanced-table/advanced-table.component';
import { AngularDataContext } from '@themost/angular';
import * as EXAMS_INSTRUCTORS_LIST_CONFIG from './exams-instructors-table.config.json';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { Subscription } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';
import { ActivatedTableService } from '../../../tables/tables.activated-table.service';

@Component({
  selector: 'app-exams-preview-instructors',
  templateUrl: './exams-preview-instructors.component.html',
  styleUrls: ['./exams-preview-instructors.component.scss']
})

export class ExamsPreviewInstructorsComponent implements OnInit, OnDestroy {

  public readonly config: AdvancedTableConfiguration = <AdvancedTableConfiguration> EXAMS_INSTRUCTORS_LIST_CONFIG;
  @ViewChild('instructors') instructors: AdvancedTableComponent;
  courseExamID: any = this._activatedRoute.snapshot.params.id;
  public recordsTotal: any;
  private fragmentSubscription: Subscription;
  @Input() reload: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _errorService: ErrorService,
    private _modalService: ModalService,
    private _toastService: ToastService,
    private _translateService: TranslateService,
  ) { }

  async ngOnInit() {
    this._activatedTable.activeTable = this.instructors;
    this.instructors.query = this._context.model(`CourseExams/${this._activatedRoute.snapshot.params.id}/instructors`)
      .asQueryable()
      .expand('instructor')
      .prepare();

    this.instructors.config = AdvancedTableConfiguration.cast(EXAMS_INSTRUCTORS_LIST_CONFIG);
    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.instructors.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  remove() {
    if (this.instructors && this.instructors.selected && this.instructors.selected.length) {
      // get items to remove
      const items = this.instructors.selected.map(item => {
        return {
          instructor: item.id,
          courseExam: this.courseExamID
        };
      });
      return this._modalService.showWarningDialog(
        this._translateService.instant('Exams.RemoveInstructorTitle'),
        this._translateService.instant('Exams.RemoveInstructorMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model('courseExamInstructors').remove(items).then(() => {
              this._toastService.show(
                this._translateService.instant('Exams.RemoveInstructorsMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Exams.RemoveInstructorsMessage.one' : 'Exams.RemoveInstructorsMessage.many')
                  , { value: items.length })
              );
              this.instructors.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}
