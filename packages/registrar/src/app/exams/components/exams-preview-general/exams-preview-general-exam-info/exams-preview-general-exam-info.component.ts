import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-exams-preview-general-exam-info',
  templateUrl: './exams-preview-general-exam-info.component.html',
  styleUrls: ['./exams-preview-general-exam-info.component.scss']
})
export class ExamsPreviewGeneralExamInfoComponent implements OnInit {

  public courseExam: any;
  public uploadGrades: any;
  public examId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {

    this.examId = this._activatedRoute.snapshot.params.id;

    this.courseExam = await this._context.model('CourseExams')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('course,examPeriod,status,completedByUser,year')
      .getItem();

  }

}
