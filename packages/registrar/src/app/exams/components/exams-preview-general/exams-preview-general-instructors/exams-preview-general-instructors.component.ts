import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-exams-preview-general-instructors',
  templateUrl: './exams-preview-general-instructors.component.html',
  styleUrls: ['./exams-preview-general-instructors.component.scss']
})
export class ExamsPreviewGeneralInstructorsComponent implements OnInit {

  courseExamID: any = this._activatedRoute.snapshot.params.id;
  public instructors: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext ) { }

  async ngOnInit() {

    this.instructors = await this._context.model('CourseExams/' + this._activatedRoute.snapshot.params.id + '/instructors')
      .asQueryable()
      .expand('instructor')
      .prepare()
      .take(-1)
      .getItems();

  }

}
