import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { ListComponent, EnumerationModelFormResolver, EnumerationModalTitleResolver } from './components/list/list.component';
import { SectionsComponent } from './components/sections/sections.component';
import {AdvancedFormModalComponent, AdvancedFormModalData, AdvancedFormItemResolver, AdvancedFormResolver} from '@universis/forms';
import { AdvancedFormModelResolver } from '@universis/forms';
import {AdvancedListComponent} from '../tables/components/advanced-list/advanced-list.component';
import {AdvancedTableConfigurationResolver} from '../tables/components/advanced-table/advanced-table-resolvers';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        data: {
            title: 'Settings'
        },
        children: [
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'sections'
            },
            {
                path: 'sections',
                component: SectionsComponent
            },
            {
                path: 'lists/AttachmentTypes',
                component: AdvancedListComponent,
                data: {
                    model: 'AttachmentTypes',
                    list: 'active',
                    description: 'Settings.Lists.AttachmentType.Description',
                    longDescription: 'Settings.Lists.AttachmentType.LongDescription',
                },
                resolve: {
                    tableConfiguration: AdvancedTableConfigurationResolver
                },
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            description: null,
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'edit',
                            closeOnSubmit: true
                        },
                        resolve: {
                            formConfig: AdvancedFormResolver,
                            data: AdvancedFormItemResolver
                        }
                    }
                ]
            },
            {
                path: 'lists/:model',
                component: ListComponent,
                children: [
                    {
                        path: 'add',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'new',
                            closeOnSubmit: true
                        },
                        resolve: {
                            model: AdvancedFormModelResolver,
                            formConfig: EnumerationModelFormResolver,
                            modalOptions: EnumerationModalTitleResolver
                        }
                    },
                    {
                        path: ':id/edit',
                        pathMatch: 'full',
                        component: AdvancedFormModalComponent,
                        outlet: 'modal',
                        data: <AdvancedFormModalData>{
                            action: 'edit',
                            closeOnSubmit: true
                        },
                        resolve: {
                            model: AdvancedFormModelResolver,
                            data: AdvancedFormItemResolver,
                            formConfig: EnumerationModelFormResolver,
                            modalOptions: EnumerationModalTitleResolver
                        }
                    }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class SettingsRoutingModule {
}
