import {TableConfiguration} from '../../../../tables/components/advanced-table/advanced-table.interfaces';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';

export class CoursesExamsConfigurationResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./courses-exams.config.${route.params.list}.json`)
            .catch( err => {
           return  import(`./courses-exams.config.list.json`);
        });
    }
}

export class CoursesExamsSearchResolver implements Resolve<TableConfiguration> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<TableConfiguration> | Promise<TableConfiguration> | TableConfiguration {
        return import(`./courses-exams.search.${route.params.list}.json`)
            .catch( err => {
                return  import(`./courses-exams.search.list.json`);
            });
    }
}

export class CoursesDefaultExamsConfigurationResolver implements Resolve<any> {
    resolve(route: ActivatedRouteSnapshot,
            state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return import(`./courses-exams.config.list.json`);
    }
}
