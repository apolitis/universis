import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';

@Component({
  selector: 'app-courses-study-programs',
  templateUrl: './courses-study-programs.component.html',
  styleUrls: ['./courses-study-programs.component.scss']
})
export class CoursesStudyProgramsComponent implements OnInit {
  public model: any;

  constructor(public _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async  ngOnInit() {
    this.model = await this._context.model('ProgramCourses')
        .where('course').equal(this._activatedRoute.snapshot.params.id)
        .expand('course,program($expand=department,studyLevel)')
        .getItems();
  }

}
