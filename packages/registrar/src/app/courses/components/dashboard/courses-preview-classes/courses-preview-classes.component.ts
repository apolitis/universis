import {Component, Injectable, OnInit, ViewChild} from '@angular/core';
import * as CLASSES_LIST_CONFIG from './courses-preview-classes.config.json';
import {
  AdvancedTableComponent,
  AdvancedTableConfiguration
} from './../../../../tables/components/advanced-table/advanced-table.component';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ActivatedTableService } from './../../../../tables/tables.activated-table.service.js';
import {Args} from '@themost/client';
import {AdvancedFormParentItemResolver} from '@universis/forms';

@Injectable({ providedIn: 'root' })
export class CourseTitleResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any>|any {
  return  new AdvancedFormParentItemResolver(this._context).resolve(route, state).then(result => {
      return result.name;
    });
  }
}


@Component({
  selector: 'app-courses-preview-classes',
  templateUrl: './courses-preview-classes.component.html',
})
export class CoursesPreviewClassesComponent implements OnInit {
  public readonly config = CLASSES_LIST_CONFIG;
  @ViewChild('classes') classes: AdvancedTableComponent;
  courseID: any = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService) { }

  async ngOnInit() {
    this._activatedTable.activeTable = this.classes;
    this.classes.query = this._context.model('courseClasses')
      .where('course').equal(this._activatedRoute.snapshot.params.id)
      .expand('course')
      .orderByDescending('year')
      .thenBy('period')
      .prepare();

    this.classes.config = AdvancedTableConfiguration.cast(CLASSES_LIST_CONFIG);
  }
}
