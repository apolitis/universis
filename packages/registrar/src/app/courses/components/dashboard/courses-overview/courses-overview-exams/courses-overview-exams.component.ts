import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-courses-overview-exams',
  templateUrl: './courses-overview-exams.component.html',
  styleUrls: ['./courses-overview-exams.component.scss']
})
export class CoursesOverviewExamsComponent implements OnInit {
  public model: any;
  public grading: any;
  @Input() currentYear: any;
  public courseId: any;

  constructor(
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {
    this.courseId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('CourseExams')
      .where('course').equal(this.courseId)
      .expand('examPeriod,status,completedByUser,year,course($expand=department)')
      .and('year').equal({'$name': '$it/course/department/currentYear'})
      .getItems();
  }

}
