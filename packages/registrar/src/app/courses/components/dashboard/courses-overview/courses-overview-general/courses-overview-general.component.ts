import {Component, Input, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'app-courses-overview-general',
  templateUrl: './courses-overview-general.component.html',
  styleUrls: ['./courses-overview-general.component.scss']
})
export class CoursesOverviewGeneralComponent implements OnInit {
  public model: any;
  public courseId: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) { }

  async ngOnInit() {

    this.courseId = this._activatedRoute.snapshot.params.id;

    this.model = await this._context.model('Courses')
      .where('id').equal(this.courseId)
      .expand('department,instructor,courseArea,gradeScale,courseStructureType,courseSector,courseCategory,replacedByCourse')
      .getItem();
  }

}
