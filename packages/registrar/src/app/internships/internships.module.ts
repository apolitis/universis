import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InternshipsHomeComponent } from './components/internships-home/internships-home.component';
import { InternshipsTableComponent } from './components/internships-table/internships-table.component';
import { InternshipsRoutingModule } from './internships.routing';
import {InternshipsSharedModule} from './internships.shared';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule, TranslateService} from '@ngx-translate/core';
import { InternshipsPreviewComponent } from './components/internships-preview/internships-preview.component';
import { InternshipsRootComponent } from './components/internships-root/internships-root.component';
import { InternshipsPreviewGeneralComponent } from './components/internships-preview-general/internships-preview-general.component';
import {FormsModule} from '@angular/forms';
import {SharedModule} from '@universis/common';
import {ElementsModule} from '../elements/elements.module';
import { StudentsSharedModule } from '../students/students.shared';
import {InternshipsAdvancedTableSearchComponent} from './components/internships-table/internships-advanced-table-search.component';
import {MostModule} from '@themost/angular';
import {BsDatepickerModule, TooltipModule} from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { AdvancedFormsModule } from '@universis/forms/lib/advanced-forms.module';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    TablesModule,
    InternshipsRoutingModule,
    InternshipsSharedModule,
    StudentsSharedModule,
    SharedModule,
    FormsModule,
    ElementsModule,
    MostModule,
    BsDatepickerModule.forRoot(),
    AdvancedFormsModule,
    RegistrarSharedModule,
    TooltipModule.forRoot()
  ],
  providers: [DatePipe],
  declarations: [InternshipsHomeComponent,
    InternshipsTableComponent,
    InternshipsPreviewComponent,
    InternshipsRootComponent,
      InternshipsPreviewGeneralComponent, InternshipsAdvancedTableSearchComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class InternshipsModule {
  constructor(private _translateService: TranslateService) { }
 }
