import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { cloneDeep, template } from 'lodash';
import * as INTERNSHIPS_LIST_CONFIG from '../internships-table/internships-table.config.list.json';

@Component({
  selector: 'app-internships-root',
  templateUrl: './internships-root.component.html',
})
export class InternshipsRootComponent implements OnInit {

  @Input() model: any;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) { }

  async ngOnInit() {
    this.model = await this._context.model('Internships')
          .where('id').equal(this._activatedRoute.snapshot.params.id)
          .expand('status,internshipPeriod,department,student($expand=person)')
          .getItem();

    // @ts-ignore
    this.config = cloneDeep(INTERNSHIPS_LIST_CONFIG as TableConfiguration);

    if (this.config.columns && this.model) {
      // get actions from config file
      this.actions = this.config.columns.filter(x => {
        return x.actions;
      })
        // map actions
        .map(x => x.actions)
        // get list items
        .reduce((a, b) => b, 0);

      // filter actions with student permissions
      this.allowedActions = this.actions.filter(x => {
        if (x.role) {
          if (x.role === 'action') {
            return x;
          }
        }
      });

      this.edit = this.actions.find(x => {
        if (x.role === 'edit') {
          x.href = template(x.href)(this.model);
          return x;
        }
      });

      this.actions = this.allowedActions;
      this.actions.forEach(action => {
        action.href = template(action.href)(this.model);
      });

    }
  }

}
