import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-internships-preview-form',
  templateUrl: './internships-preview-form.component.html'
})
export class InternshipsPreviewFormComponent implements OnInit {

  @Input() model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {

  }

}
