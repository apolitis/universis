import {Component, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {AdvancedTableComponent, AdvancedTableDataResult} from '../../../tables/components/advanced-table/advanced-table.component.js';
import {AdvancedSearchFormComponent} from '../../../tables/components/advanced-search-form/advanced-search-form.component';

import { Router, ActivatedRoute } from '@angular/router';
import {Subscription} from 'rxjs';
import {AdvancedTableSearchComponent} from '../../../tables/components/advanced-table/advanced-table-search.component';
import {ActivatedTableService} from '../../../tables/tables.activated-table.service';


@Component({
  selector: 'app-scholarships-table',
  templateUrl: './scholarships-table.component.html',
  styleUrls: ['./scholarships-table.component.scss']
})
export class ScholarshipsTableComponent implements OnInit, OnDestroy  {

  private dataSubscription: Subscription;
  private paramSubscription: Subscription;

  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;

  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;

  public recordsTotal: any;

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        this.table.config = data.tableConfiguration;
        this.advancedSearch.getQuery().then( res => {
          this.table.destroy();
          this.table.query = res;
          this.advancedSearch.text = '';
          this.table.fetch(false);
        });
      }
    });
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }


  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

}
