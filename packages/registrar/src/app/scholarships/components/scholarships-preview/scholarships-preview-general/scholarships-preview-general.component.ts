import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-scholarships-preview-general',
  templateUrl: './scholarships-preview-general.component.html'
})
export class ScholarshipsPreviewGeneralComponent implements OnInit {

  public model: any;
  public studentScholarships: any;
  public scholarshipID = this._activatedRoute.snapshot.params.id;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Scholarships')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department', 'organization')
      .getItem();

    this.studentScholarships = await this._context.model('StudentScholarships')
      .where('scholarship').equal(this._activatedRoute.snapshot.params.id)
      .expand('Student($expand=person,studentStatus,inscriptionYear,inscriptionPeriod)')
      .orderByDescending('grade')
      .getItems();
  }

}
