import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {cloneDeep} from 'lodash';
import * as SCHOLARSHIPS_LIST_CONFIG from '../scholarships-table/scholarships-table.config.list.json';
import {TemplatePipe} from '@universis/common';

@Component({
  selector: 'app-scholarships-root',
  templateUrl: './scholarships-root.component.html',

})
export class ScholarshipsRootComponent implements OnInit {
  public model: any;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _template: TemplatePipe) {
  }

  async ngOnInit() {
    this.model = await this._context.model('Scholarships')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('department', 'organization')
      .getItem();

    // @ts-ignore
  this.config = cloneDeep(SCHOLARSHIPS_LIST_CONFIG as TableConfiguration);

  if (this.config.columns && this.model) {
    // get actions from config file
    this.actions = this.config.columns.filter(x => {
      return x.actions;
    })
      // map actions
      .map(x => x.actions)
      // get list items
      .reduce((a, b) => b, 0);

    // filter actions with student permissions
    this.allowedActions = this.actions.filter(x => {
      if (x.role) {
        if (x.role === 'action') {
          return x;
        }
      }
    });

    this.edit = this.actions.find(x => {
      if (x.role === 'edit') {
        x.href = this._template.transform(x.href, this.model);
        return x;
      }
    });

    this.actions = this.allowedActions;
    this.actions.forEach(action => {
      action.href = this._template.transform(action.href, this.model);
    });

  }
  }
}
