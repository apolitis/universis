import {Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, ViewChild} from '@angular/core';
import 'rxjs/add/observable/combineLatest';
import {DocumentSignComponent} from '../../../reports-shared/components/document-sign/document-sign.component';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AngularDataContext} from '@themost/angular';
import {ErrorService, LoadingService, ModalService, ToastService} from '@universis/common';
import {SignerService} from '../../../reports-shared/services/signer.service';
import {HttpClient} from '@angular/common/http';
import {AppEventService} from '@universis/common/shared/services/app-event.service';
import {Observable, Subscription} from 'rxjs';

declare var $: any;

@Component({
    selector: 'app-document-sign-action',
    templateUrl: './document-sign-action.component.html',
    styles: [
        `
      @keyframes FadeIn {
        from {
          color: #664BF1;
        }

        to {
          background-color: #FFF;
        }
      }

      .loading-icon {
        color: white;
        animation: FadeIn 3s ease-in-out forwards infinite;
      }
    `
    ]
})
export class DocumentSignActionComponent extends DocumentSignComponent implements OnInit, OnDestroy {

    @ViewChild('progress') progress: ElementRef;
    @Input() items: any;
    @Input() refresh: any = new EventEmitter<any>();
    @Input() execute: Observable<any>;
    @Input() description: string;
    private refreshSubscription: Subscription;
    private executeSubscription: Subscription;

    constructor(router: Router,
                activatedRoute: ActivatedRoute,
                _translateService: TranslateService,
                _context: AngularDataContext,
                _errorService: ErrorService,
                _signer: SignerService,
                _modalService: ModalService,
                _http: HttpClient,
                _appEvent: AppEventService,
                _toastService: ToastService,
                _loadingService: LoadingService) {
        super(router, activatedRoute, _translateService,
            _context, _errorService, _signer,
            _modalService, _http, _appEvent,
            _toastService, _loadingService);
        // set button text
        this.okButtonText = 'Requests.Edit.Start';
    }

    async ok() {
        try {
            this.loading = true;
            this.lastError = null;
            // do action
            this.execute.subscribe((result) => {
                this.loading = false;
                this.refresh.emit({
                    progress: 100
                });
                if (result && result.errors && result.errors > 0) {
                    // show message for partial success
                    if (result.errors === 1) {
                        this.lastError = new Error(this._translateService.instant(
                            'Requests.Edit.CompletedWithErrors.Description.One'));
                    } else {
                        this.lastError = new Error(this._translateService.instant(
                            'Requests.Edit.CompletedWithErrors.Description.Many',
                            result));
                    }
                    this.refresh.emit({
                        progress: 1
                    });
                    $(this.progress.nativeElement).css('visibility', 'hidden');
                    return;
                }
                if (this._modalService.modalRef) {
                    this._modalService.modalRef.hide();
                }
            }, (err) => {
                this.loading = false;
                this.lastError = err;
                this.refresh.emit({
                    progress: 0
                });
            });
        } catch (err) {
            this.loading = false;
            this.lastError = err;
        }
    }

    async ngOnInit(): Promise<void> {
        this.refreshSubscription = this.refresh.subscribe((value) => {
            if (value && value.progress) {
                let progress = 0;
                if (value.progress < 0) {
                    progress = 0;
                } else if (value.progress > 100) {
                    progress = 100;
                } else {
                    progress = value.progress;
                }
                $(this.progress.nativeElement).find('.progress-bar').css('width', `${progress}%`);
                $(this.progress.nativeElement).css('visibility', 'visible');
            }
        });
        return super.ngOnInit();
    }
    // tslint:disable-next-line:use-life-cycle-interface
    ngOnDestroy() {
        super.ngOnDestroy();
        if (this.refreshSubscription) {
            this.refreshSubscription.unsubscribe();
        }
        if (this.executeSubscription) {
            this.executeSubscription.unsubscribe();
        }
    }

}
