import {Component, Input, OnInit} from '@angular/core';

@Component({
    // tslint:disable-next-line:component-selector
    selector: 'form-collapsible',
    styles: [
        `
            .form-collapsible {
            }
        `
    ],
    template: `
        <div class="card shadow-none border border-gray-200 text-secondary mb-0">
            <div class="card-body px-3 py-3">
                <a href="javascript:void(0)" class="card-title font-lg font-weight-normal  text-decoration-none text-secondary"
                   (click)="collapsed = !collapsed">
                    <span>
                        <ng-content select="form-header"></ng-content>
                    </span>
                    <i class="collapsible ml-3 pt-1 fa" [ngClass]=" {'fa-chevron-up' : !collapsed, 'fa-chevron-down' : collapsed}"></i>
                </a>
                <div *ngIf="!collapsed">
                    <ng-content select="form-body"></ng-content>
                </div>
            </div>
        </div>
    `
})
/**
 * @description Implements a collapsible card component.
 * @example
 <form-collapsible [collapsed]="false">
     <form-header>Form title</form-header>
     <form-body>
         <form class="mt-2 d-none collapsible-form">
            <div>
              <div class="row">
                <div class="col-3">
                <div class="form-group text-dark">
                    <label class="text-dark" for="firstName">First Name</label>
                        <input disabled="disabled"
 class="form-control bg-white text-secondary"
 id="firstName" type="text" placeholder="Enter your first name" value="Chelsea">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group text-dark">
                        <label class="text-dark" for="lastName">Last Name</label>
                        <input disabled="disabled"
 class="form-control bg-white text-secondary"
 id="lastName" type="text" placeholder="Enter your last name" value="Ross">
                    </div>
                </div>
                <div class="col-3">
                    <div class="form-group text-dark">
                        <label class="text-dark" for="jobTitle">Last Name</label>
                        <input disabled="disabled"
 class="form-control bg-white text-secondary" id="jobTitle"
 type="text" placeholder="Enter your job title" value="Accountant">
                </div>
            </div>
              </div>
            </div>
        </form>
     </form-body>
 </form-collapsible>
 */
export class FormCollapsibleComponent implements OnInit {

    /**
     * Gets or sets a boolean which indicates whether this card is collapsed or
     */
    @Input() collapsed = true;

    ngOnInit(): void {
        //
    }

}
