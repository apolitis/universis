import {NgModule} from '@angular/core';
import {NavigationExtras, RouterModule, Routes} from '@angular/router';
import {ThesesHomeComponent} from './components/theses-home/theses-home.component';
import {ThesesTableComponent} from './components/theses-table/theses-table.component';
import {ThesesRootComponent} from './components/theses-root/theses-root.component';
import {ThesesPreviewComponent} from './components/theses-preview/theses-preview.component';
import {AdvancedFormRouterComponent} from '../registrar-shared/advanced-form-router/advanced-form-router.component';
import { AdvancedFormItemResolver, AdvancedFormModalComponent, AdvancedFormModalData } from '@universis/forms';
import {StudentsSharedModule} from '../students/students.shared';
import {ThesesAddStudentComponent} from './components/theses-dashboard/theses-dashboard-students/theses-add-student.component';
import {
  ThesesTableConfigurationResolver,
  ThesesTableSearchResolver
} from './components/theses-table/theses-table-config.resolver';
import {ThesesSharedModule} from './theses.shared';
import {ThesesDashboardComponent} from './components/theses-dashboard/theses-dashboard.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardOverviewComponent} from './components/theses-dashboard/theses-dashboard-overview/theses-dashboard-overview.component';
// tslint:disable-next-line:max-line-length
import {ThesesDashboardStudentsComponent} from './components/theses-dashboard/theses-dashboard-students/theses-dashboard-students.component';
import {ThesesDashboardGradesComponent} from './components/theses-dashboard/theses-dashboard-grades/theses-dashboard-grades.component';
import {
  ActiveDepartmentResolver,
  CurrentAcademicPeriodResolver,
  CurrentAcademicYearResolver
} from '../registrar-shared/services/activeDepartmentService.service';





const routes: Routes = [
    {
        path: '',
        component: ThesesHomeComponent,
        data: {
            title: 'Theses'
        },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'list/active'
        },
        {
          path: 'list',
          pathMatch: 'full',
          redirectTo: 'list/index'
        },
        {
          path: 'list/:list',
          component: ThesesTableComponent,
          data: {
            title: 'StudentTheses'
          },
          resolve: {
            tableConfiguration: ThesesTableConfigurationResolver,
            searchConfiguration: ThesesTableSearchResolver
          },

        }
      ]
    },
  {
    path: 'create',
    component: ThesesRootComponent,
    children: [
      {
        path: 'new',
        pathMatch: 'full',
        component: AdvancedFormRouterComponent,
        data: {
          status: {
            alternateName: 'potential'
          } ,
          startDate: new Date(),
          continue:
            ['/theses/${id}/dashboard/students',
              {outlets: {modal: 'add'}}
            ]
        }
      }
    ],
    resolve: {
      department: ActiveDepartmentResolver,
      startYear : CurrentAcademicYearResolver,
      startPeriod : CurrentAcademicPeriodResolver
    }
  },
  {
      path: ':id',
      component: ThesesRootComponent,
      data: {
        title: 'Theses Home'
      },
      children: [
        {
          path: '',
          pathMatch: 'full',
          redirectTo: 'dashboard'
        },
        {
          path: 'dashboard',
          component: ThesesDashboardComponent,
          data: {
            title: 'Theses Dashboard'
          },
          children: [
            {
              path: '',
              redirectTo: 'general'
            },
            {
              path: 'general',
              component: ThesesDashboardOverviewComponent,
              data: {
                title: 'Theses Preview General'
              }
            },
            {
              path: 'students',
              component: ThesesDashboardStudentsComponent,
              data: {
                title: 'Theses Students'
              },
              children: [
                {
                  path: 'add',
                  pathMatch: 'full',
                  component: ThesesAddStudentComponent,
                  outlet: 'modal',
                  data: <AdvancedFormModalData> {
                    title: 'Theses.AddStudent',
                    config: StudentsSharedModule.ActiveStudentsList,
                  },
                  resolve: {
                    thesis: AdvancedFormItemResolver
                  }
                }
              ]
            },
            {
              path: 'grades',
              component: ThesesDashboardGradesComponent,
              data: {
                title: 'Theses Preview Grades'
              }
            }
           ]
        },
        {
          path: ':action',
          component: AdvancedFormRouterComponent,
          data:
        {
          serviceParams: {
            $expand: 'instructor'
          }
        }
        }
      ]
  }
];

@NgModule({
    imports: [RouterModule.forChild(routes),
    ThesesSharedModule],
    exports: [RouterModule],
    declarations: []
})
export class ThesesRoutingModule {
}
