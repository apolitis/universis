import { Component, EventEmitter, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

import * as THESES_INSTRUCTORS_LIST_CONFIG from './theses-preview-instructos.config.list.json'
import { Subscription } from 'rxjs';
import { DIALOG_BUTTONS, ErrorService, ModalService, ToastService } from '@universis/common';
import { ActivatedTableService} from '../../../../tables/tables.activated-table.service';



import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-theses-dashboard-instructors',
  templateUrl: './theses-dashboard-grades.component.html',
})
export class ThesesDashboardGradesComponent implements OnInit {
  public model: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _activatedTable: ActivatedTableService,
              private _errorService: ErrorService,
              private _context: AngularDataContext) {}

  async ngOnInit() {


    this.model = await this._context.model('Theses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('instructor,type,status,students($expand=student($expand=department,person,studentStatus))')
      .getItem();

  }

}
