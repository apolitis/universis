import {Component, Input, OnInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';

@Component({
  selector: 'app-theses-dashboard-grades-instructors-form',
  templateUrl: './theses-dashboard-grades-instructors-form.component.html'
})
export class ThesesDashboardGradesInstructorsFormComponent implements OnInit {

  @Input() studentId: any;
  public grades: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.grades = await  this._context.model('StudentThesisResults')
      .where('thesis').equal(this._activatedRoute.snapshot.params.id)
      .and('student').equal(this.studentId)
      .expand('instructor')
      .getItems();
  }

}
