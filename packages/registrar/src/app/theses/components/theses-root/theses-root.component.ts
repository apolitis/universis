import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {AngularDataContext} from '@themost/angular';
import {TranslateService} from '@ngx-translate/core';
import { cloneDeep, template } from 'lodash';
import * as THESES_LIST_CONFIG from '../theses-table/theses-table.config.json';
import { TableConfiguration } from '../../../tables/components/advanced-table/advanced-table.interfaces';

@Component({
  selector: 'app-theses-root',
  templateUrl: './theses-root.component.html',
})
export class ThesesRootComponent implements OnInit {
  public model: any;
  public isCreate = false;
  public tabs: any[];
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _translate: TranslateService,
              private _context: AngularDataContext) {}

  async ngOnInit() {

    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    this.model = await this._context.model('Theses')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .expand('instructor,type,status,students($expand=student($expand=department,person))')
      .getItem();

    // @ts-ignore
    this.config = cloneDeep(THESES_LIST_CONFIG as TableConfiguration);

    if (this.config.columns && this.model) {
      // get actions from config file
      this.actions = this.config.columns.filter(x => {
        return x.actions;
      })
        // map actions
        .map(x => x.actions)
        // get list items
        .reduce((a, b) => b, 0);

      // filter actions with student permissions
      this.allowedActions = this.actions.filter(x => {
        if (x.role) {
          if (x.role === 'action') {
            return x;
          }
        }
      });

      this.edit = this.actions.find(x => {
        if (x.role === 'edit') {
          x.href = template(x.href)(this.model);
          return x;
        }
      });

      this.actions = this.allowedActions;
      this.actions.forEach(action => {
        action.href = template(action.href)(this.model);
      });
    }
  }
}
