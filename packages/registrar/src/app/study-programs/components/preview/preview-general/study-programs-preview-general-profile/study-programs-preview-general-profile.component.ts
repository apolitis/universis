import { Component, OnInit } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-study-programs-preview-general-profile',
  templateUrl: './study-programs-preview-general-profile.component.html',
  styleUrls: ['./study-programs-preview-general-profile.component.scss']
})
export class StudyProgramsPreviewGeneralProfileComponent implements OnInit {

  public model: any;
  public studyProgramID: any;

  constructor( private _activatedRoute: ActivatedRoute,
               private _context: AngularDataContext) { }

  async ngOnInit() {
    this.studyProgramID = this._activatedRoute.snapshot.params.id;
    this.model = await this._context.model('StudyPrograms')
    .where('id').equal(this._activatedRoute.snapshot.params.id)
    .expand('department,studyLevel,gradeScale')
    .getItem();

  }

}
