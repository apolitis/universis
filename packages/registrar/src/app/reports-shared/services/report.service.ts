import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import cloneDeep = require('lodash/cloneDeep');
import * as REPORT_PARAMETERS from './report-parameters.json';
import { ServiceUrlPreProcessor } from '@universis/forms';
import { ActiveDepartmentService } from '../../registrar-shared/services/activeDepartmentService.service';
import { ResponseError } from '@themost/client';
import { HttpClient } from '@angular/common/http';

export declare interface BlobContent {
  contentLocation: string;
  contentLanguage?: string;
  contentEncoding?: string;
  contentDisposition?: string;
}

@Injectable()
export class ReportService {

  constructor(private _context: AngularDataContext,
              private _activeDepartment: ActiveDepartmentService,
              private _http: HttpClient) {
    //
  }

  /**
   * Reads report parameters
   */
  readReport(id: any): Promise<any> {
    return this._context.model(`ReportTemplates/${id}/read`).asQueryable().getItem();
  }
  /**
   * Gets a report template
   * @param {number} id
   */
  getReport(id: any): Promise<any> {
    return this._context.model('ReportTemplates')
      .where('id').equal(id)
      .expand('reportCategory')
      .getItem();
  }

  async getReportFormFor(report: any) {
    const form  = <any>cloneDeep(REPORT_PARAMETERS);
    const columns = form.components[0].components[0].columns;
    new ServiceUrlPreProcessor(this._context).parse(form);
    // add extra parameters
    const params = {
      department: await this._activeDepartment.getActiveDepartment()
    };
    form.params = form.params || {};
    Object.assign(form.params, params);
    return form;
  }

  /**
   * Prints a report template
   * @param {number} id
   * @param {*} reportParams
   */
  async printReport(id: number, reportParams: any): Promise<Blob> {
    // todo: pass accept content type based on export type provided by the user
    const headers = new Headers({
      'Accept': 'application/pdf',
      'Content-Type': 'application/json'
    });
    // get service headers
    const serviceHeaders = this._context.getService().getHeaders();
    // manually assign service headers
    Object.keys(serviceHeaders).forEach((key) => {
      if (Object.prototype.hasOwnProperty.call(serviceHeaders, key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    // get print url
    const printURL = this._context.getService().resolve(`ReportTemplates/${id}/print`);
    // get report blob
    return this._http.post(printURL, reportParams, {
      headers: this._context.getService().getHeaders(),
      responseType: 'blob',
      observe: 'response'
    }).toPromise().then( (response) => {
      const contentLocation = response.headers.get('content-location');
      if (contentLocation != null) {
        Object.defineProperty(response.body, 'contentLocation', {
          configurable: true,
          enumerable: true,
          writable: true,
          value: contentLocation
        });
      }
      return response.body;
    });
  }

}
