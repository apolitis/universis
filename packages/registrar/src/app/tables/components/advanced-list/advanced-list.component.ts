import { Component, OnInit, ViewChild, OnDestroy, Injectable, Input } from '@angular/core';
import { AngularDataContext, AngularDataService } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
// tslint:disable-next-line: max-line-length
import { AdvancedTableComponent, AdvancedTableDataResult, AdvancedTableConfiguration } from '../advanced-table/advanced-table.component';
import { ErrorService, DIALOG_BUTTONS, ModalService, UserActivityService } from '@universis/common';
import {ResponseError} from '@themost/client';
import { TableConfiguration } from '../advanced-table/advanced-table.interfaces';
import * as LIST_CONFIG from './report-template.config.list.json';
import cloneDeep = require('lodash/cloneDeep');
import { ToastService } from '@universis/common';

@Component({
  selector: 'app-advanced-list',
  templateUrl: './advanced-list.component.html'
})
export class AdvancedListComponent implements OnInit, OnDestroy {

  public recordsTotal: number;
  public config: TableConfiguration;
  private subscription: Subscription;
  private dataSubscription: Subscription;
  private reloadSubscription: Subscription;
  @Input('entitySet') entitySet: string;
  @Input('category') category: string;
  @Input('description') description: string;
  @Input('longDescription') longDescription: string;
  @Input('tableConfiguration') tableConfiguration: any;
  @Input('searchConfiguration') searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _activatedRoute: ActivatedRoute,
    private _errorService: ErrorService,
    private _toastService: ToastService,
    private _modalService: ModalService,
    private _userActivityService: UserActivityService
    ) { }
  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.reloadSubscription) {
      this.reloadSubscription.unsubscribe();
    }
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async ngOnInit() {
    try {
      // get metadata
      const metadata = await this._context.getMetadata();
      // get route data
      this.dataSubscription = this._activatedRoute.data.subscribe( routeData => {
        // get entityType
        const re = new RegExp(`^${routeData.model}$`, 'ig');
        const entitySet = metadata.EntityContainer.EntitySet.find( x => {
          return re.test(x.Name);
        });
        // if entity type is null
        if (entitySet == null) {
          // navigate to error
          return this._errorService.navigateToError(new ResponseError('EntitySet Not Found', 404));
        }
        // get entity type
        const entityType = metadata.EntityType.find( x => {
          return x.Name === entitySet.EntityType;
        });
        if (entityType == null) {
          return this._errorService.navigateToError(new ResponseError('EntityType Not Found', 404));
        }
        if (routeData.searchConfiguration) {
          this.searchConfiguration = routeData.searchConfiguration;
        }
        if (routeData.tableConfiguration) {
          this.tableConfiguration = routeData.tableConfiguration;
        }
        if (routeData.description) {
          this.description = this._translateService.instant(routeData.description);
        }
        if (routeData.longDescription) {
          this.longDescription = this._translateService.instant(routeData.longDescription);
        }
        if (routeData.category) {
          this.category = this._translateService.instant(routeData.category);
        }
        // clone default configuration
        const defaultConfiguration = AdvancedTableConfiguration.cast(this.tableConfiguration, true);
        // correct edit button link
        const column = defaultConfiguration.columns.find( col => {
          return col.formatter === 'ButtonFormatter' && col.formatString === '(modal:edit)';
        });
        const url: Array<string> = [];
        this._activatedRoute.snapshot.pathFromRoot.forEach( route => {
          url.push.apply(url, route.url);
        });
        column.formatString = `#/${url.join('/')}/(modal:\$\{id\}/edit)`;
        // create list configuration by assigning list entity set
        this.table.config = Object.assign({
          model: entitySet.Name
        }, defaultConfiguration);
        // fetch data
        this.table.fetch(true);
        // do reload by using hidden fragment e.g. /classes#reload
        this.reloadSubscription = this._activatedRoute.fragment.subscribe(fragment => {
          if (fragment && fragment === 'reload') {
            this._toastService.show(
              this.description,
              this._translateService.instant('Settings.OperationCompleted')
            );
            this.table.fetch(true);
          }
        });
        // finally add user activity
        this._userActivityService.setItem({
          category: this.category,
          description: this.description,
          url: window.location.hash.substring(1),
          dateCreated: new Date
        });
      });
    } catch (err) {
      console.error(err);
      return this._errorService.showError(err);
    }
  }


  /**
   * Data load event handler of advanced table component
   * @param {AdvancedTableDataResult} data
   */
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  /**
   * Remove selected items
   */
  remove() {
    if (this.table && this.table.selected && this.table.selected.length) {
      // get items to remove
      const items = this.table.selected;
      return this._modalService.showWarningDialog(
        this._translateService.instant('Tables.RemoveItemsTitle'),
        this._translateService.instant('Tables.RemoveItemsMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this._context.model(this.table.config.model).remove(items).then(() => {
              this._toastService.show(
                this.description + ':' + this._translateService.instant('Tables.RemoveItemMessage.title'),
                this._translateService.instant((items.length === 1 ?
                  'Tables.RemoveItemMessage.one' : 'Tables.RemoveItemMessage.many')
                  , { value: items.length })
              );
              this.table.fetch(true);
            }).catch(err => {
              this._errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}
